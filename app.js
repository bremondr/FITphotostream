var http = require('http');
var fs = require('fs');
var jsonfile = require('jsonfile');
var path = require('path');

var express = require('express');
var expressHbs = require('express3-handlebars');

var Filter = require('bad-words');
var filter = new Filter({ placeHolder: '~'});

//var slova = jsonfile.readFileSync('./bad-words.json');
//console.log(slova.cj);

var dataFile = './display.json';

// --------------------------------------------------------    
// Social networks dependencies
// --------------------------------------------------------    

var Instafeed = require("instafeed.js");
var Instagram = require('instagram-node').instagram();
var FB = require('fb');

// --------------------------------------------------------    
// Web app setup
// --------------------------------------------------------

var app = express();
app.engine('hbs', expressHbs({extname:'hbs', defaultLayout:'main.hbs'}));
app.set('view engine', 'hbs');
app.use(express.static(path.join(__dirname, 'public')));

var WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({ port: 5000 });

// --------------------------------------------------------    
// Data variables
// --------------------------------------------------------
var streamSources = {};
streamSources.tags = ['fit_ctu', 'fit_cvut'];

var displayData = {};
displayData.updated = 0;
displayData.images = [];
displayData.lastIDs = [];

// --------------------------------------------------------    
// Insta API tokens    
// -------------------------------------------------------- 
var instagram_client_id = 'e75a59129d5a4b0dacb780a3db6460c5';
var instagram_client_secret = 'd6bcfdb51d3b41e6ae861813d06dc41e';

var insta_token = '3108735378.1677ed0.db6685d1334b4305957332fa87dc7f3d';
//'3108735378.1677ed0.db6685d1334b4305957332fa87dc7f3d'

Instagram.use({ access_token: '3108735378.1677ed0.db6685d1334b4305957332fa87dc7f3d' });
//Instagram.use({
//  client_id: instagram_client_id,
//  client_secret: instagram_client_secret
//});
//var tmp_id = '467ede5a6b9b48ae8e03f4e2582aeeb3';
//instagram_client_id = tmp_id;

// --------------------------------------------------------    
// functions    
// -------------------------------------------------------- 


    var download = function(url, dest, targetFolder, cb) {
      //console.log(targetFolder);
      if(!fs.existsSync(targetFolder))    //TODO: check if the folder exists create if not with 
          fs.mkdirSync(targetFolder);
        
      var file = fs.createWriteStream(dest);
      var request = http.get(url, function(response) {
        response.pipe(file);
        file.on('finish', function() {
          file.close(cb(dest));  // close() is async, call cb after close completes.
        });
      }).on('error', function(err) { // Handle errors
        fs.unlink(dest); // Delete the file async. (But we don't check the result)
        if (cb) cb(err.message);
      });
    }; 

    function uniques(arr) {
        var a = [];
        for (var i=0, l=arr.length; i<l; i++)
            if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
                a.push(arr[i]);
        return a;
    }

    var map = function(nr, min, max){
        if(nr < 15)
            return 1;
        if(nr < 25)
            return 2;
        if(nr < 35)
            return 3;
        else
            return 4;
        
    }

/*
   Function: readInstaData

   Gets all the Instagram media specified by given tag

   Parameters:

      data - Array of Instagram media objects.
      sub - Substring used for subfolder sorting.
      
   Returns:  
   
      res - JSON object reresenting simplified media object, with added info about where to store it.
      
*/    
    var readInstaData = function(data, sub){
        var res = { data: [] };   
        //res.data = [];        
        for(i in data){
            if(data[i].created_time > displayData.updated){
                var img = {};
                var u = data[i].images.standard_resolution.url;
                var strToReplace = 's'+data[i].images.standard_resolution.height+'x'+data[i].images.standard_resolution.height;
                img.id          = data[i].id;
                img.caption     = filter.clean(data[i].caption.text) || "";
                img.url         = u.replace(strToReplace, 's1080x1080').replace('https', 'http');
                img.likes       = data[i].likes.count;
                img.timeStamp   = data[i].created_time;
                img.target      = './data/'+sub+'/'+data[i].id+'.jpg';            
                img.likesRatio  = map(data[i].likes.count, 1, 4);   
                res.data.push(img); 
//                if(!img.caption.includes("~")){
//                    res.data.push(img);        
//                    
//                } else {
//                    console.log('skipping because of rude word');
//                }
            }
        }            
        return res;
    }

    var getMediaByTag = function(tag, callback){
        console.log(tag);
        Instagram.tag_media_recent(tag, function(err, medias, pagination, remaining, limit) {
            console.log(err);
            if(medias){
                var data = readInstaData(medias, tag).data;
                console.log(tag+" "+data.length);
                for(i in data){
                    download(data[i].url, data[i].target, './data/'+tag+'/', function(filename){console.log('file '+filename+' closed');})
                }
                displayData.images = displayData.images.concat(data);
                //pagination.next();
            }
            callback;
            jsonfile.writeFileSync('./data/display.json', displayData, {spaces: 2});
        });         
    };


/*
   Function: processInstagramResponse

   Gets all the Instagram media specified by given tag

   Parameters:

      err - Error message from Instagram.
      medias - Object containing the media from instagram in the form of { media: []}.
      pagination - Object containing the callback function for next set of media, given from Instagram
      remaining - Some number given back by instagram
      limit - Some number given back by instagram
      tag - The tag that was used for the query
      
   See Also:

      <readInstaData> 
      <download>
      
*/
    var processInstagramResponse = function( err, medias, pagination, remaining, limit, tag, first ) {
        if(err)
            console.log('err: '+err);
        if(medias){                
            var data = readInstaData(medias, tag).data;
//            if(first){
//                displayData.lastIDs.push({tag: data[0].id});
//            }
            console.log(tag+" "+data.length);
            for(i in data){
                download(data[i].url, data[i].target, './data/'+tag+'/', function(filename){console.log('file '+filename+' closed');})
            }
            
            if(data.length > 0){
                displayData.images = displayData.images.concat(data);  
                displayData.updated = Math.floor(Date.now() / 1000); 
                jsonfile.writeFileSync(dataFile, displayData, {spaces: 2});
                wss.broadcast({'updated': true});
            }            
        }                    

        if(pagination.next) {
            pagination.next(function( err, medias, pagination, remaining, limit ){
                processInstagramResponse( err, medias, pagination, remaining, limit, tag, false);
            }); // Will get second page results 
        }
    };
/*
   Function: getAllMediaByTag

   Gets all the Instagram media specified by given tag

   Parameters:

      tag - The tag to be used for the query.
      
   See Also:

      <processInstagramResponse>
*/    
    var getAllMediaByTag = function(tag){
//        Instagram.tag(tag, function(err, result, remaining, limit) {
//            console.log(result);
//        });
        console.log(tag);
        Instagram.tag_media_recent(tag, function( err, medias, pagination, remaining, limit ){
            processInstagramResponse( err, medias, pagination, remaining, limit, tag, true);
        } );         
    };    
/*
   Function: getAllMediaByUserID

   Gets all the Instagram media from specified user

   Parameters:

      id - The user ID to be used for the query.

   See Also:

      <processInstagramResponse>
*/     
    var getAllMediaByUserID = function(id){
//        Instagram.user(id, function(err, result, remaining, limit){
//            console.log(result);
//        });
        //console.log(id);
        Instagram.user_media_recent(id, function( err, medias, pagination, remaining, limit ){
            processInstagramResponse( err, medias, pagination, remaining, limit, id, true);
        } );         
    };  
/*
   Function: getAllMediaByUserName

   Gets all the Instagram media from specified user

   Parameters:

      username - The username to be used for the query.

   See Also:

      <getAllMediaByUserID>
*/
    var getAllMediaByUserName = function(username){
        Instagram.user_search(username, function(err, users, remaining, limit) {
            console.log(users[0].id);
            getAllMediaByUserID(users[0].id);
        });        
    }

    var getMediaByUserID = function(id){
        console.log(id);
        Instagram.user_media_recent(id, function(err, medias, pagination, remaining, limit) {
            if(medias){
                var data = readInstaData(medias, id).data
                console.log(data.length);
                for(i in data){
                    download(data[i].url, data[i].target, './data/'+id+'/', function(){console.log('file '+data[i].target+' closed');})
                }
                displayData.images = displayData.images.concat(data);
            }       
            jsonfile.writeFileSync('./data/display.json', displayData, {spaces: 2});
        });        
    };

    
    var updateStream = function(){
        
        //displayData = removeOldEntries(displayData);
        //getAllMediaByUserName('c1754041');
        getAllMediaByTag('c1754041_test');
        console.log('update finished');
    }
if(fs.existsSync(dataFile))
    displayData = jsonfile.readFileSync(dataFile);    
    
//getMediaByTag('fit_cvut');
//getMediaByTag('fit_ctu');
//getAllMediaByTag('fitcvut');
//getAllMediaByTag('sagelab');
//getMediaByTag('fitctu');  
//getAllMediaByUserID('1699870105');

//getAllMediaByUserName('c1754041');
//getAllMediaByTag('c1754041_test');
//getAllMediaByUserName('fit_ctu');

// --------------------------------------------------------    
// REST API    
// --------------------------------------------------------    

// ------------- TESTING PURPOSE ---------------------------
app.get('/', function(req, res){
  res.render('index');
});    
  
app.get('/demo', function(req, res){
    Instagram.tag_media_recent('fitcvut', function(err, medias, pagination, remaining, limit) {
        //console.log(medias);
        if(medias){
            var data = {};
            data.images = readInstaData(medias, 'fitcvut').data;
            console.log(data.images.length);
//            for(i in data){
//                download(data[i].url, data[i].target, function(){console.log('file '+data[i].target+' closed');})
//            }
            //res.end( JSON.stringify(data) );
            res.render('demo', data);
        }
        else
            res.end('err fitcvut');
    });    
});
    
app.get('/FITstream', function( req, res){
    console.log(displayData);
    var data = { images: uniques(displayData.images) };
    res.render('demo', data);
});

app.put('/FITstream/tag', function(req, res){
    // TODO
    //streamSources.push(req.query.data);
//    Instagram.add_tag_subscription('fitcvut', 'http://MYHOST/tag/funny', function(err, result, remaining, limit){
//        console.log(err);
//        res.end(err.body);
//        console.log(result);
//    });
//    res.end('succes');
    res.end('TODO'); 
});
    
app.get('/tag/:name', function (req, res) {
    console.log(req.url);    
    res.end('TODO'); 
}); 


app.get('/fitcvut', function (req, res) {
    Instagram.user_search('fit_ctu', function(err, users, remaining, limit) {
        console.log(users);
    });
//    Instagram.tag_media_recent('fitcvut', function(err, medias, pagination, remaining, limit) {
//        //console.log(medias);
//        if(medias){
//            var data = readInstaData(medias, 'fitcvut').data
//            console.log(data.length);
//            for(i in data){
//                download(data[i].url, data[i].target, function(){console.log('file '+data[i].target+' closed');})
//            }
//            res.end( JSON.stringify(data) );
//        }
//        else
//            res.end('err fitcvut');
//    });
//    Instagram.user_media_recent('1699870105', function(err, medias, pagination, remaining, limit) {
//        if(medias){
//            var data = readInstaData(medias, 'fit_ctu').data
//            console.log(data.length);
//            for(i in data){
//                download(data[i].url, data[i].target, function(){console.log('file '+data[i].target+' closed');})
//            }
//            res.end( JSON.stringify(data) );
//        }
//        else
//            console.log(err);
//            res.end( '' );        
//        
//    });
});        


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
// ------------- TESTING PURPOSE ---------------------------

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
 
  ws.send('connection started');
});

wss.broadcast = function broadcast(data) {
  console.log(data); 
  wss.clients.forEach(function each(client) {
     
    client.send(data);
  });
};
//
//                console.log('test before timeout');
//                setTimeout(suspend.resume(), 10000);
//                console.log('test after timeout');


var interval = setInterval(function () {
        updateStream();   
    

    
}, 30000);
